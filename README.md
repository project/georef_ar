# GeoRefAR Field

<img width="300px" height="auto" src="https://datosgobar.github.io/georef-ar-api/images/logo.svg"></img>

GeoRefAr module provides integration with GeoRef-AR API for handling addresses of Argentina.

[API of the Argentine Geographical Data Standardization Service!](https://datosgobar.github.io/georef-ar-api)

Note: This module is an independent integration of the mentioned API.

### Current Features
- Provides a GeoRefAr field type.
- Configurable levels: Province, Department, Locality, Street and Street number.

## REQUIREMENTS
The module has not any kind of special requirement.

## INSTALLATION
Please use the standard Drupal module installation or

`composer require drupal/georef_ar`

## CONFIGURATION
1. Add a new GeoRefAr field type to an entity.
2. On the field settings form select the address levels to show.

## MAINTAINERS

Current maintainers:
* Matias Miranda - https://www.drupal.org/u/matiasmiranda
