<?php

namespace Drupal\georef_ar;

/**
 * Enumerates available geo ref levels.
 */
final class GeoRefArLevels {

  const PROVINCE = 'province';
  const DEPARTMENT = 'department';
  const LOCALITY = 'locality';
  const STREET = 'street';
  const STREET_NUMBER = 'street_number';

  /**
   * Static cache of available values.
   *
   * @var array
   */
  protected static $values = [];

  /**
   * Gets all available values.
   *
   * @return array
   *   The available values, keyed by constant.
   *
   * @throws \ReflectionException
   */
  public static function getAll() {
    $class = get_called_class();
    if (empty(static::$values[$class])) {
      $reflection = new \ReflectionClass($class);
      static::$values[$class] = $reflection->getConstants();
    }

    return static::$values[$class];
  }

}
