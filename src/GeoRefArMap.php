<?php

/**
 * @file
 * Contains \Drupal\georef_ar\GeoRefArMap.
 */

namespace Drupal\georef_ar;

use Drupal\Core\TypedData\TypedData;

// @todo complete this implementation of Map.
/**
 * A computed map for a GeoRef.
 */
class GeoRefArMap extends TypedData {

  /**
   * Cached processed value.
   *
   * @var string|null
   */
  protected $processed = NULL;

  /**
   * Implements \Drupal\Core\TypedData\TypedDataInterface::getValue().
   */
  public function getValue($langcode = NULL) {
    if ($this->processed !== NULL) {
      return $this->processed;
    }

    $item = $this->getParent();

    $this->processed = '{{MAP OF ' . $item->province . '}}';
    return $this->processed;
  }

  /**
   * Implements \Drupal\Core\TypedData\TypedDataInterface::setValue().
   */
  public function setValue($value, $notify = TRUE) {
    $this->processed = $value;

    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->name);
    }
  }

}
