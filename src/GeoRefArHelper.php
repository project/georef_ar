<?php

namespace Drupal\georef_ar;

/**
 * Enumerates available geo ref levels label.
 */
class GeoRefArHelper {

  /**
   * Gets levels label.
   *
   * @return array
   *   Array of translatable label markup keyed by GeoRefArLevel.
   */
  public static function getLevelsLabel() {
    return [
      GeoRefArLevels::PROVINCE => t('Province'),
      GeoRefArLevels::DEPARTMENT => t('Department'),
      GeoRefArLevels::LOCALITY => t('Locality'),
      GeoRefArLevels::STREET => t('Street'),
      GeoRefArLevels::STREET_NUMBER => t('Street number'),
    ];
  }

}
