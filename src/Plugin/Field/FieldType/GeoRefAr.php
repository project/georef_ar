<?php

/**
 * @file
 * Contains \Drupal\georef_ar\Plugin\Field\FieldType\GeoRefAr.
 */

namespace Drupal\georef_ar\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\georef_ar\GeoRefArHelper;
use Drupal\georef_ar\GeoRefArLevels;

/**
 * Plugin implementation of the 'GeoRef AR' field type.
 *
 * @FieldType (
 *   id = "georef_ar",
 *   label = @Translation("GeoRef AR"),
 *   description = @Translation("Stores a geo data from georef-ar-api."),
 *   default_widget = "georef_ar",
 *   default_formatter = "georef_ar"
 * )
 */
class GeoRefAr extends FieldItemBase {

  const LEVEL_HIDDEN = 'hidden';

  const LEVEL_OPTIONAL = 'optional';

  const LEVEL_REQUIRED = 'required';

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'province' => [
          'description' => 'Province ID.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ],
        'department' => [
          'description' => 'Department ID.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ],
        'locality' => [
          'description' => 'Locality ID.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ],
        'street' => [
          'description' => 'Street name if any.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => NULL,
        ],
        'street_number' => [
          'description' => 'Street Number if any.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => NULL,
        ],
        'data' => [
          'description' => 'JSON Data of the geo location selected.',
          'type' => 'text',
          'size' => 'big',
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // @todo implement logic
    return parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties[GeoRefArLevels::PROVINCE] = DataDefinition::create('string')
      ->setLabel(t('Province'))
      ->setDescription(t('Province'));

    $properties[GeoRefArLevels::DEPARTMENT] = DataDefinition::create('string')
      ->setLabel(t('Department'))
      ->setDescription(t('Department'));

    $properties[GeoRefArLevels::LOCALITY] = DataDefinition::create('string')
      ->setLabel(t('Locality'))
      ->setDescription(t('Locality'));

    $properties[GeoRefArLevels::STREET] = DataDefinition::create('string')
      ->setLabel(t('Street'))
      ->setDescription(t('Street'));

    $properties[GeoRefArLevels::STREET_NUMBER] = DataDefinition::create('string')
      ->setLabel(t('Number'))
      ->setDescription(t('Street number'));

    $properties['data'] = DataDefinition::create('string')
      ->setLabel(t('JSON Data'))
      ->setDescription(t('JSON Data'));

    $properties['map'] = DataDefinition::create('any')
      ->setLabel(t('Map'))
      ->setDescription(t('A representation on a map of the region selected'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\georef_ar\GeoRefArMap');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return parent::defaultFieldSettings() + [
        'level_config' => [],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element['level_config'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Level'),
        $this->t('Config'),
      ],
      '#element_validate' => [[get_class($this), 'levelConfigValidate']],
    ];

    $level_configs = $this->getLevelConfig();
    foreach (GeoRefArHelper::getLevelsLabel() as $field_name => $label) {
      $level_config = isset($level_configs[$field_name]) ? $level_configs[$field_name] : self::LEVEL_OPTIONAL;
      $element['level_config'][$field_name] = [
        'level_label' => [
          '#type' => 'markup',
          '#markup' => $label,
        ],
        'config' => [
          '#type' => 'select',
          '#options' => [
            self::LEVEL_HIDDEN => $this->t('Hidden'),
            self::LEVEL_OPTIONAL => $this->t('Optional'),
            self::LEVEL_REQUIRED => $this->t('Required'),
          ],
          '#default_value' => $level_config,
        ],
      ];
    }

    return $element;
  }

  /**
   * Form element validation handler to set dependent level configs.
   *
   * @param array $element
   *   The element level.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the entire form.
   */
  public static function levelConfigValidate(array $element, FormStateInterface $form_state) {
    $configs = $form_state->getValue($element['#parents']);

    foreach ($configs as $level => &$data) {
      if ($level == GeoRefArLevels::PROVINCE && $data['config'] == self::LEVEL_HIDDEN) {
        $configs[GeoRefArLevels::DEPARTMENT]['config'] = self::LEVEL_HIDDEN;
      }

      if ($level == GeoRefArLevels::DEPARTMENT && $data['config'] == self::LEVEL_HIDDEN) {
        $configs[GeoRefArLevels::LOCALITY]['config'] = self::LEVEL_HIDDEN;
      }

      if ($level == GeoRefArLevels::LOCALITY && $data['config'] == self::LEVEL_HIDDEN) {
        $configs[GeoRefArLevels::STREET]['config'] = self::LEVEL_HIDDEN;
      }

      if ($level == GeoRefArLevels::STREET && $data['config'] == self::LEVEL_HIDDEN) {
        $configs[GeoRefArLevels::STREET_NUMBER]['config'] = self::LEVEL_HIDDEN;
      }
    }

    $form_state->setValue($element['#parents'], $configs);
  }

  /**
   * Gets the level config for the current field.
   *
   * @return array
   *   Level config constants keyed by Geo Ref AR Level constants.
   */
  public function getLevelConfig() {
    $level_config = [];
    $level_configs = $this->getSetting('level_config');
    foreach ($level_configs as $level => $data) {
      $level_config[$level] = $data['config'];
    }

    return $level_config;
  }

}
