<?php

/**
 * @file
 * Contains \Drupal\georef_ar\Plugin\Field\FieldWidget\GeoRefArWidget.
 */

namespace Drupal\georef_ar\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\georef_ar\GeoRefArLevels;
use Drupal\georef_ar\Plugin\Field\FieldType\GeoRefAr;
use Drupal\georef_ar\Plugin\Field\GeoRefArBase;
use Drupal\georef_ar\Plugin\Field\GeoRefArTrait;
use Drupal\georef_ar\Services\GeoRefArApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Plugin implementation of the 'GeoRef AR' widget.
 *
 * @FieldWidget (
 *   id = "georef_ar",
 *   label = @Translation("GeoRef AR widget"),
 *   field_types = {
 *     "georef_ar"
 *   }
 * )
 */
class GeoRefArWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The Geo Ref AR Api service.
   *
   * @var \Drupal\georef_ar\Services\GeoRefArApiService
   */
  protected $api_service;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, GeoRefArApiService $apiService) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->api_service = $apiService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\WidgetPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('georefar.api.service')
    );
  }

  /**
   * Level config
   *
   * @var mixed
   */
  protected $level_config;

  /**
   * @inheritDoc
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getFieldStorageDefinition()
      ->getName();
    $item_values = $items[$delta]->toArray();
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element)) {
      if ($this->isDefaultValueWidget($form_state)) {
        $field_values = $form_state->getValue('default_value_input')[$field_name];
      }
      else {
        $field_values = $form_state->getValue($field_name);
      }
      $item_values = $field_values[$delta];
    }

    $provinces = $this->getFieldOptions(NULL, 'provinces');
    $departments = [];
    $localities = [];

    if (!empty($item_values[GeoRefArLevels::PROVINCE])) {
      $departments = $this->getFieldOptions($item_values[GeoRefArLevels::PROVINCE], 'departments');
    }

    if (!empty($item_values[GeoRefArLevels::DEPARTMENT])) {
      $localities = $this->getFieldOptions($item_values[GeoRefArLevels::DEPARTMENT], 'localities');
    }

    $element += [
      '#type' => 'details',
      '#open' => TRUE,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element[GeoRefArLevels::PROVINCE] = [
      '#type' => 'select',
      '#title' => $this->t('Province'),
      '#options' => ['' => $this->t('- Select -')] + $provinces,
      '#default_value' => $item_values[GeoRefArLevels::PROVINCE],
      '#ajax' => [
        'callback' => [$this, 'getDepartmentField'],
        'event' => 'change',
        'wrapper' => 'georef-ar-department-delta-' . $delta,
      ],
      '#access' => $this->isLevelVisible(GeoRefArLevels::PROVINCE),
      '#required' => $this->isLevelRequired(GeoRefArLevels::PROVINCE),
    ];

    $element[GeoRefArLevels::DEPARTMENT] = [
      '#type' => 'select',
      '#title' => $this->t('Department'),
      '#options' => ['' => $this->t('- Select -')] + $departments,
      '#default_value' => $item_values[GeoRefArLevels::DEPARTMENT],
      '#ajax' => [
        'callback' => [$this, 'getLocalityField'],
        'event' => 'change',
        'wrapper' => 'georef-ar-locality-delta-' . $delta,
      ],
      '#prefix' => '<div id="georef-ar-department-delta-' . $delta . '">',
      '#suffix' => '</div>',
      '#states' => [
        'invisible' => [
          ':input[name="' . $field_name . '[' . $delta . '][' . GeoRefArLevels::PROVINCE . ']"]' => ['value' => ''],
        ],
        'disabled' => [
          ':input[name="' . $field_name . '[' . $delta . '][' . GeoRefArLevels::PROVINCE . ']"]' => ['value' => ''],
        ],
      ],
      '#access' => $this->isLevelVisible(GeoRefArLevels::DEPARTMENT),
      '#required' => $this->isLevelRequired(GeoRefArLevels::DEPARTMENT),
    ];

    $element[GeoRefArLevels::LOCALITY] = [
      '#type' => 'select',
      '#title' => $this->t('Locality'),
      '#options' => ['' => $this->t('- Select -')] + $localities,
      '#default_value' => $item_values[GeoRefArLevels::LOCALITY],
      '#prefix' => '<div id="georef-ar-locality-delta-' . $delta . '">',
      '#suffix' => '</div>',
      '#states' => [
        'invisible' => [
          ':input[name="' . $field_name . '[' . $delta . '][' . GeoRefArLevels::DEPARTMENT . ']"]' => ['value' => ''],
        ],
        'disabled' => [
          ':input[name="' . $field_name . '[' . $delta . '][' . GeoRefArLevels::DEPARTMENT . ']"]' => ['value' => ''],
        ],
      ],
      '#access' => $this->isLevelVisible(GeoRefArLevels::LOCALITY),
      '#required' => $this->isLevelRequired(GeoRefArLevels::LOCALITY),
    ];

    $element[GeoRefArLevels::STREET] = [
      '#type' => 'textfield',
      '#title' => $this->t('Street'),
      '#default_value' => $item_values[GeoRefArLevels::STREET] ?? '',
      '#access' => $this->isLevelVisible(GeoRefArLevels::STREET),
      '#required' => $this->isLevelRequired(GeoRefArLevels::STREET),
    ];

    $element[GeoRefArLevels::STREET_NUMBER] = [
      '#type' => 'textfield',
      '#title' => $this->t('Street number'),
      '#default_value' => $item_values[GeoRefArLevels::STREET_NUMBER] ?? '',
      '#access' => $this->isLevelVisible(GeoRefArLevels::STREET_NUMBER),
      '#required' => $this->isLevelRequired(GeoRefArLevels::STREET_NUMBER),
    ];

    // Ensure levels are optional on the default value widget.
    if ($this->isDefaultValueWidget($form_state)) {
      foreach (GeoRefArLevels::getAll() as $level) {
        $element[$level]['#after_build'][] = [
          get_class($this),
          'makeLevelOptional',
        ];
      }
    }

    return $element;
  }

  /**
   * @inheritDoc
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if ($this->isDefaultValueWidget($form_state)) {
      return $values;
    }

    $submit_handlers = $form_state->getSubmitHandlers();
    if (!in_array('::submitForm', $submit_handlers)) {
      return $values;
    }

    foreach ($values as $delta => $data) {
      foreach ($values[$delta] as $level => $level_value) {
        if ($level == '_original_delta') {
          continue;
        }
        $method = 'get' . ucfirst($level);
        if (method_exists($this->api_service, $method)) {
          $values[$delta]['data'][$level] = $this->api_service->{$method}($level_value);
        }
        else {
          $values[$delta]['data'][$level] = new \stdClass();
          $values[$delta]['data'][$level]->id = $level_value;
          $values[$delta]['data'][$level]->nombre = $level_value;
        }
      }
      $values[$delta]['data'] = json_encode($values[$delta]['data']);
    }

    return $values;
  }

  /**
   * Form API callback to make level optional.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return array
   *   Updated Element array form.
   */
  public static function makeLevelOptional(array $element, FormStateInterface $form_state) {
    $element['#required'] = FALSE;
    $element['#access'] = TRUE;
    return $element;
  }

  /**
   * Ajax Callback to return Department level field.
   *
   * @param array $form
   *   The array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return array|mixed|null
   *   Array form piece of ajax response.
   */
  public function getDepartmentField(array $form, FormStateInterface $form_state) {
    return $this->getField($form, $form_state, GeoRefArLevels::DEPARTMENT);
  }

  /**
   * Ajax Callback to return Locality level field.
   *
   * @param array $form
   *   The array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return array|mixed|null
   *   Array form piece of ajax response.
   */
  public function getLocalityField(array $form, FormStateInterface $form_state) {
    return $this->getField($form, $form_state, GeoRefArLevels::LOCALITY);
  }

  /**
   * Ajax Callback Helper to return a form element piece of a field_wrapper.
   *
   * @param array $form
   *   The array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   * @param $field_wrapper
   *   The field_wrapper.
   *
   * @return array|mixed|null
   *   Array form piece
   */
  public function getField(array $form, FormStateInterface $form_state, $field_wrapper) {
    $triggering_element = $form_state->getTriggeringElement();
    $array_parents = $triggering_element['#array_parents'];
    array_pop($array_parents);
    $array_parents[] = $field_wrapper;
    return NestedArray::getValue($form, $array_parents);
  }

  /**
   * Gets field options of the given type.
   *
   * @param $parent
   *   The parent level id.
   * @param $type
   *   The type of Level.
   *
   * @return array
   *   An array keyed by Level id of the given type.
   */
  public function getFieldOptions($parent, $type) {
    $params = [
      'provinces' => [
        'apiCall' => 'getProvinces',
        'list' => 'provincias',
      ],
      'departments' => [
        'apiCall' => 'getDepartments',
        'list' => 'departamentos',
      ],
      'localities' => [
        'apiCall' => 'getLocalities',
        'list' => 'localidades',
        //        'parents' => [
        //          'localidad_censal',
        //          'nombre'
        //        ]
      ],
      'streets' => [
        'apiCall' => 'getStreets',
        'list' => 'calles',
      ],
    ];
    $_values = NULL;

    $method = 'get' . ucfirst($type);
    if (method_exists($this->api_service, $method)) {
      $_values = $this->api_service->{$method}($parent);
    }
    $_values = !empty($_values->{$params[$type]['list']}) ? $_values->{$params[$type]['list']} : [];
    $values = [];
    foreach ($_values as $value) {
      $value = json_decode(json_encode($value), TRUE);
      if (!empty($params[$type]['parents'])) {
        $values[$value['id']] = NestedArray::getValue($value, $params[$type]['parents']);
      }
      else {
        $values[$value['id']] = $value['nombre'];
      }

    }

    return $values;
  }

  /**
   * Checks if the given level is visible.
   *
   * @param $level
   *   The GeoRefArLevel.
   *
   * @return bool
   *   TRUE if the level is visible, FALSE otherwise.
   */
  protected function isLevelVisible($level) {
    if (empty($this->level_config)) {
      $this->level_config = $this->fieldDefinition->getSetting('level_config');
    }
    if (!empty($this->level_config[$level])) {
      return ($this->level_config[$level]['config'] !== GeoRefAr::LEVEL_HIDDEN);
    }

    return FALSE;
  }

  /**
   * Checks if the given level is required.
   *
   * @param $level
   *   The GeoRefArLevel.
   *
   * @return bool
   *   TRUE if the level is required, FALSE otherwise.
   */
  protected function isLevelRequired($level) {
    if (empty($this->level_config)) {
      $this->level_config = $this->fieldDefinition->getSetting('level_config');
    }
    if (!empty($this->level_config[$level])) {
      return ($this->level_config[$level]['config'] === GeoRefAr::LEVEL_REQUIRED);
    }

    return FALSE;
  }

}
