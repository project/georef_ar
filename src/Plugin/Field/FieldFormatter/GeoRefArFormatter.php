<?php

/**
 * @file
 * Contains \Drupal\georef_ar\Plugin\Field\FieldFormatter\GeoRefArFormatter.
 */

namespace Drupal\georef_ar\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\georef_ar\GeoRefArLevels;

/**
 * Plugin implementation of the 'GeoRef AR' formatter.
 *
 * @FieldFormatter (
 *   id = "georef_ar",
 *   label = @Translation("GeoRef AR"),
 *   field_types = {
 *     "georef_ar"
 *   }
 * )
 */
class GeoRefArFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $data = json_decode($item->data);
      $markup = [];
      foreach (GeoRefArLevels::getAll() as $level) {
        if (!empty($data->{$level}->nombre)) {
          if ($level == GeoRefArLevels::STREET_NUMBER && !empty($markup[GeoRefArLevels::STREET])) {
            $markup[GeoRefArLevels::STREET] .= ' ' . $data->{$level}->nombre;
            continue;
          }
          $markup[$level] = $data->{$level}->nombre;
        }
      }
      $markup = join(', ', $markup);

      // @todo provide a configuration form the set the desired output.
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $markup,
      ];
    }

    return $elements;
  }

}
