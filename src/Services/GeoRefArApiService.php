<?php

namespace Drupal\georef_ar\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactory;

class GeoRefArApiService {

  /**
   * Endpoint to get all provinces ordered by name.
   */
  const API_PROVINCES = 'https://apis.datos.gob.ar/georef/api/provincias?orden=nombre';

  /**
   * Endpoint to get all departments of a province ordered by name.
   */
  const API_DEPARTMENTS = 'https://apis.datos.gob.ar/georef/api/departamentos?provincia={{PROVINCE}}&max=1000&orden=nombre';

  /**
   * Endpoint to get all localities of a department ordered by name.
   */
  const API_LOCALITIES = 'https://apis.datos.gob.ar/georef/api/localidades?departamento={{DEPARTMENT}}&max=1000&orden=nombre';

  /**
   * Endpoint to get all streets of a locality ordered by name.
   */
  const API_STREETS = 'https://apis.datos.gob.ar/georef/api/calles?localidad_censal={{LOCALITY}}&max=1000&orden=nombre';

  /**
   * Endpoint to get a province by province id.
   */
  const API_PROVINCE = 'https://apis.datos.gob.ar/georef/api/provincias?id={{PROVINCE_ID}}';

  /**
   * Endpoint to get a department by department id.
   */
  const API_DEPARTMENT = 'https://apis.datos.gob.ar/georef/api/departamentos?id={{DEPARTMENT_ID}}';

  /**
   * Endpoint to get a locality by locality id.
   */
  const API_LOCALITY = 'https://apis.datos.gob.ar/georef/api/localidades?id={{LOCALITY_ID}}';

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * GeoRefArApiService constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *    The cache backend.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $channelFactory
   *    The logger.
   */
  public function __construct(CacheBackendInterface $cache, LoggerChannelFactory $channelFactory) {
    $this->cache = $cache;
    $this->logger = $channelFactory->get('georef_ar');
  }

  /**
   * Gets provinces from GeoRefAr API.
   *
   * @return bool|mixed
   *   Json Object response or FALSE on failure.
   */
  public function getProvinces() {
    $cid = 'georefar.api.provinces';
    $cached = $this->cache->get($cid);
    if (!empty($cached->data)) {
      return $cached->data;
    }

    $data = $this->apiRequest(self::API_PROVINCES);
    $this->cache->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, [$cid]);
    return $data;
  }

  /**
   * Gets a province by province_id from GeoRefAr API.
   *
   * @return bool|mixed
   *   Json Object response or FALSE on failure.
   */
  public function getProvince($province_id) {
    $cid = 'georefar.api.province.' . $province_id;
    $cached = $this->cache->get($cid);
    if (!empty($cached->data)) {
      return $cached->data;
    }

    $url = str_replace('{{PROVINCE_ID}}', $province_id, self::API_PROVINCE);
    $data = $this->apiRequest($url);
    $data = $data->provincias[0] ?? NULL;
    $this->cache->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, [
      'georefar.api.province',
      $cid,
      'georefar.api.provinces',
    ]);

    return $data;
  }

  /**
   * Gets departments from GeoRefAr API.
   *
   * @return bool|mixed
   *   Json Object response or FALSE on failure.
   */
  public function getDepartments($province_id) {
    $cid = 'georefar.api.departments.' . $province_id;
    $cached = $this->cache->get($cid);
    if (!empty($cached->data)) {
      return $cached->data;
    }

    $url = str_replace('{{PROVINCE}}', $province_id, self::API_DEPARTMENTS);
    $data = $this->apiRequest($url);
    $this->cache->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, [
      'georefar.api.departments',
      $cid,
    ]);
    return $data;
  }

  /**
   * Gets a department by department_id from GeoRefAr API.
   *
   * @return bool|mixed
   *   Json Object response or FALSE on failure.
   */
  public function getDepartment($department_id) {
    $cid = 'georefar.api.department.' . $department_id;
    $cached = $this->cache->get($cid);
    if (!empty($cached->data)) {
      return $cached->data;
    }

    $url = str_replace('{{DEPARTMENT_ID}}', $department_id, self::API_DEPARTMENT);
    $data = $this->apiRequest($url);
    $data = $data->departamentos[0] ?? NULL;
    $this->cache->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, [
      'georefar.api.department',
      $cid,
      'georefar.api.departments',
    ]);

    return $data;
  }

  /**
   * Gets localities from GeoRefAr API.
   *
   * @return bool|mixed
   *   Json Object response or FALSE on failure.
   */
  public function getLocalities($department_id) {
    $cid = 'georefar.api.localities.' . $department_id;
    $cached = $this->cache->get($cid);
    if (!empty($cached->data)) {
      return $cached->data;
    }

    $url = str_replace('{{DEPARTMENT}}', $department_id, self::API_LOCALITIES);
    $data = $this->apiRequest($url);
    $this->cache->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, [
      'georefar.api.localities',
      $cid,
    ]);
    return $data;
  }

  /**
   * Gets a locality by locality_id from GeoRefAr API.
   *
   * @return bool|mixed
   *   Json Object response or FALSE on failure.
   */
  public function getLocality($locality_id) {
    $cid = 'georefar.api.locality.' . $locality_id;
    $cached = $this->cache->get($cid);
    if (!empty($cached->data)) {
      return $cached->data;
    }

    $url = str_replace('{{LOCALITY_ID}}', $locality_id, self::API_LOCALITY);
    $data = $this->apiRequest($url);
    $data = $data->localidades[0] ?? NULL;
    $this->cache->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, [
      'georefar.api.locality',
      $cid,
      'georefar.api.localities',
    ]);

    return $data;
  }

  /**
   * Gets streets by locality_id from GeoRefAr API.
   *
   * @return bool|mixed
   *   Json Object response or FALSE on failure.
   */
  public function getStreets($locality_id) {
    $cid = 'georefar.api.streets.' . $locality_id;
    $cached = $this->cache->get($cid);
    if (!empty($cached->data)) {
      return $cached->data;
    }

    $url = str_replace('{{LOCALITY}}', $locality_id, self::API_STREETS);
    $data = $this->apiRequest($url);
    $this->cache->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, [
      'georefar.api.streets',
      $cid,
    ]);
    return $data;
  }

  /**
   * Performs a request to GeoRefAr API.
   *
   * @return bool|mixed
   *   Json Object response or FALSE on failure.
   */
  public function apiRequest($url) {
    try {
      $response = \Drupal::httpClient()
        ->get($url, [
          'verify' => FALSE,
          'timeout' => 30,
          'headers' => [
            'Content-Type' => 'application/json',
          ],
        ]);
      $data = $response->getBody();
      if (empty($data)) {
        $this->logger->error('GeoRefArApiService::apiRequest(): Error requesting @url, empty data returned.', ['@url' => self::API_PROVINCES]);
        return FALSE;
      }
    } catch (RequestException $e) {
      $this->logger->error('GeoRefArApiService::apiRequest(): Request exception @message.', ['@message' => $e->getMessage()]);
      return FALSE;
    }

    $data = $data->getContents();
    if ($data) {
      return json_decode($data);
    }

    return $data;
  }

}
